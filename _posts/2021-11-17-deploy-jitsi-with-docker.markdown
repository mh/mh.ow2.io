---
layout: post
title:  "Deploy Jitsi with Docker"
categories: jitsi docker
---

I've been involved in installing and configuring a new Jitsi instance at [Hivane](https://www.hivane.net). I mainly followed the [official deployment documentation](https://jitsi.github.io/handbook/docs/devops-guide/devops-guide-docker). The goal was also to test [Jibri](https://github.com/jitsi/jibri) features like recording and streaming.

How to install Docker and Docker-Compose isn't covered here, for this refer to the documentation at Docker:
* [Docker](https://docs.docker.com/engine/install/)
* [Docker Compose](https://docs.docker.com/compose/install/)

On my side I installed the above on a dedicated VM and a fresh Debian 10 (Buster). To run the stack I'm using a dedicated user which belongs to the `docker` group.

# TL;DR inspired by the official doc
1. Download and extract the [latest release](https://github.com/jitsi/docker-jitsi-meet/releases/latest). DO NOT clone the git repository. See below if you are interested in running test images.

1. Create a .env file by copying and adjusting env.example:

        cp env.example .env

1. Set strong passwords in the security section options of .env file by running the following bash script

       ./gen-passwords.sh

1. Create required CONFIG directories

        mkdir -p ~/.jitsi-meet-cfg/{web/crontabs,web/letsencrypt,transcripts,prosody/config,prosody/prosody-plugins-custom,jicofo,jvb,jigasi,jibri}

    At this point *do not start* the stack yet. We're going to prepare the host for Jibri. Basically it's about loading the module `snd-aloop`:

1. configure 5 capture/playback interfaces

        echo "options snd-aloop enable=1,1,1,1,1 index=0,1,2,3,4" > /etc/modprobe.d/alsa-loopback.conf

1. setup autoload the module

        echo "snd-aloop" >> /etc/modules

1. Reboot and see if the module was loaded with:

        lsmod | grep snd_aloop

1. Finally, start the stack. In this setup we also leverage Jibri:

        docker-compose -f docker-compose.yml -f jibri.yml up -d

I provide below the values I tweaked in `.env`. Of course it doesn't include the `XX_PASSWORD` variables that you should have set with the script `./gen-passwords.sh` from the step 2 above.

{% highlight diff %}
--- env.example	2021-10-21 15:20:10.000000000 +0200
+++ .env	2021-10-28 16:22:26.779779243 +0200
@@ -27,0 +28 @@
+DISABLE_REACTIONS=1
@@ -36 +37 @@
-HTTP_PORT=8000
+HTTP_PORT=80
@@ -39 +40 @@
-HTTPS_PORT=8443
+HTTPS_PORT=443
@@ -45 +46 @@
-#PUBLIC_URL=https://meet.example.com
+PUBLIC_URL=https://jitsi2.hivane.net
@@ -78 +79 @@
-#ENABLE_LETSENCRYPT=1
+ENABLE_LETSENCRYPT=1
@@ -81 +82 @@
-#LETSENCRYPT_DOMAIN=meet.example.com
+LETSENCRYPT_DOMAIN=jitsi2.hivane.net
@@ -84 +85 @@
-#LETSENCRYPT_EMAIL=alice@atlanta.net
+LETSENCRYPT_EMAIL=certbot@ow2.org
@@ -258 +259 @@
-JVB_TCP_HARVESTER_DISABLED=true
+JVB_TCP_HARVESTER_DISABLED=false
@@ -324 +325 @@
-#ENABLE_RECORDING=1
+ENABLE_RECORDING=1
@@ -376 +377 @@
-#ENABLE_HTTP_REDIRECT=1
+ENABLE_HTTP_REDIRECT=1
{% endhighlight %}

Everything else in `.env` remains untouched

Changes overview:
- `DISABLE_REACTIONS` disable the reactions menu from the UI.
- `HTTP_PORT` and `HTTPS_PORT` are set to standard values
- `PUBLIC_URL`, the base URL of the instance
- `ENABLE_LETSENCRYPT`, `LETSENCRYPT_DOMAIN` (same as `PUBLIC_URL` above), `LETSENCRYPT_EMAIL` : enable automatic certificates management
- `JVB_TCP_HARVESTER_DISABLED` : enable Jitsi Video Bridge fallback to TCP when UDP isn't available
- `ENABLE_RECORDING` enable record *and* streaming
- `ENABLE_HTTP_REDIRECT` : http redirects to https

# Report on testing
## Recording

In release `5963` we had an issue with recording because of the resolution (1080p) which were overloading the server memory and caused `ffmpeg` to be killed after about a minute. With the new version (`6433`), the issue is solved as they lowered the default resolution to 720p (and it can be configured ! :sun_with_face: ).

Recording is working properly, in 720p, since release `6433`. The MPEG-4 video files are stored on the filesystem under `~/.jitsi-meet-cfg/jibri/recordings`.

## Streaming

YouTube streaming seems to work and I've been able to stream to PeerTube @ https://tube.cloud-libre.eu as well.

For PeerTube streaming, on [thing to remember](https://docs.joinpeertube.org/use-create-upload-video): on Jitsi side, in place of "Youtube live stream key" you should "_paste the PeerTube RTMP URL followed by the PeerTube live stream key (you may have to add a slash in-between)._"
